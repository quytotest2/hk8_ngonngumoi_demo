const express = require('express');
const router = express.Router();

require('dotenv').config()
const variableData = process.env.variableData || 'NodeJS Code'

router.get('/', function(req, res, next) {
  res.send({
    name: 'node-docker-gitlab-ci',
    server: 'express',
    variableData: variableData
  });
});



// expected output: Array [5, 44, "foo"]

router.get('/Resolving-JavaScript-Promises', function(req, res, next) {
  let promise1 = Promise.resolve(5);
  let promise2 = 44;
  let promise3 = new Promise(function(resolve, reject) {
    setTimeout(resolve, 100, 'foo');
  });
  
  Promise.all([promise1, promise2, promise3]).then(function(values) {
    res.send({
      name: 'node-docker-gitlab-ci',
      server: 'express',
      variableData: variableData,
      result: values
    });
  });
});


module.exports = router;
